#------------------------------------------------------------\
#  @2022 - 22 Nov 
#  Createed By A. Toto Priyono
#  MAIN APP
#
#  Demo Implementasi Plate Detection & Plate Recognition
#  Using Yolo & Tensorflow - CNN Algorithm
#-----------------------------------------------------------\

# IMPORT ALL LIBRARY

from flask import Flask, render_template, request
import os
from deeplearning import get_tnkb_yolo, segment_characters, get_image_char, show_results
from flask_mysqldb import MySQL
from datetime import datetime


# INIT WEB SERVER & DB SERVER
app = Flask(__name__)

app.config['MYSQL_HOST']="localhost"
app.config['MYSQL_USER']="toto"
app.config['MYSQL_PASSWORD']="admin"
app.config['MYSQL_PORT']=3308
app.config['MYSQL_DB']="license_detection"

mysql = MySQL(app)


# INIT GLOBAL VARIABLE 
BASE_PATH = os.getcwd()
UPLOAD_PATH = os.path.join(BASE_PATH,'static/foto_upload')

# DEFINE ROUTING AND HANDLER FUNCTION 

# - ROUTING TO HOME PAGE
@app.route('/')
def index():
    return render_template('index.html')

# - ROUTING TO PREDIKSI PAGE
@app.route('/prediksi', methods=['POST','GET'])
def prediksi():
    if request.method == 'POST':
        upload_file = request.files['image_name']
        filename = upload_file.filename
        path_save = os.path.join(UPLOAD_PATH,filename)
        upload_file.save(path_save)
        
        # deteksi posisi pelat dan ambil image pelat
        roi,confidences_np = get_tnkb_yolo(path_save)
        
        # baca image pelat dan extract image karakter
        img_char=segment_characters(roi,filename)
        
        # memprediksi karakter dari image char
        prediksi_plat_number=show_results(img_char)
        prediksi_plat_number=list(prediksi_plat_number)
        
        # ambil dan simpan image char ke folder
        list_images_char=get_image_char(img_char,filename)
        
        now = datetime.now()
        formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

        plat_number_str=''
        propinsi_code=''
        
        for idx,x in enumerate(prediksi_plat_number):
            plat_number_str=plat_number_str + x
            if (not x.isdigit() and idx <4 ):
               propinsi_code = propinsi_code + x
               print('propinsi code: ',propinsi_code)
                
        cur = mysql.connection.cursor()            
        #sql_query="INSERT INTO `license_detection`.`trx_test_data` (`trx_date_time`, `trx_ori_img`, `trx_plate_img`, `trx_conf_level_plate`, `trx_plate_text`) VALUES ('" + str(formatted_date) + "', '/static/foto_upload/" + filename +"', '/static/hasil_prediksi/tnkb/" + filename +"', '"+ str(confidences_np) +"', '"+ plat_number_str + "')"
        #sql_query="INSERT INTO `license_detection`.`trx_test_data` (`trx_date_time`, `trx_ori_img`, `trx_plate_img`,`trx_plate_clean`,`trx_plate_countur`, `trx_conf_level_plate`, `trx_plate_text`) VALUES ('" + str(formatted_date) + "', '/static/foto_upload/" + filename +"', '/static/hasil_prediksi/tnkb/" + filename +"','/static/hasil_prediksi/countur/" + filename +"', '/static/hasil_prediksi/segmentasi/" + filename +"','"+ str(confidences_np) +"', '"+ plat_number_str + "')"
        sql_query="INSERT INTO `license_detection`.`trx_test_data` (`trx_date_time`, `trx_ori_img`, `trx_plate_img`,`trx_plate_clean`,`trx_plate_countur`, `trx_conf_level_plate`, `trx_plate_text`,`trx_prov_kode`)" \
            "  VALUES ('" + str(formatted_date) + "', '/static/foto_upload/" + filename +"', '/static/hasil_prediksi/tnkb/" + filename +"','/static/hasil_prediksi/countur/" + filename +"', '/static/hasil_prediksi/segmentasi/" + filename +"','"+ str(confidences_np) +"', '"+ plat_number_str + "','"+ propinsi_code +"')" 
        
        sql_query_prov="SELECT prov_name FROM prov_indonesia WHERE prov_kode='" + propinsi_code + "'"
        
        print(sql_query)
        cur.execute(sql_query)
        mysql.connection.commit()
        cur.close()
        
        cur2 = mysql.connection.cursor()
        cur2.execute(sql_query_prov)
        data_prov=cur2.fetchall()

        print('data prov ', data_prov)

        if len(data_prov):
            nama_daerah=data_prov[0][0]
        else:
            nama_daerah="kendaraan tidak dikenali daerahnya dari mana.."
        
        print('nama daerah :',nama_daerah)    
        cur2.close()
        
        return render_template('prediksi.html',
                               upload=True,
                               upload_image=filename,
                               conf_level=confidences_np,
                               images_char=list_images_char,
                               prediksi_plat_number=prediksi_plat_number,propinsi_code=propinsi_code,nama_daerah=nama_daerah)
    else:
        return render_template('prediksi.html',upload=False)

# - ROUTING TO LOG DETEKSI PAGE
@app.route('/log_deteksi')
def log_deteksi():
    cur = mysql.connection.cursor()
    sql_query= "SELECT trx_test_data.*, prov_indonesia.prov_name " \
    "FROM trx_test_data " \
    "INNER JOIN  prov_indonesia " \
    "ON  trx_test_data.trx_prov_kode=prov_indonesia.prov_kode ORDER BY trx_test_data.trx_id DESC"
            
    cur.execute(sql_query)
    data_trx=cur.fetchall()
    cur.close()  
    
    return render_template('log_deteksi.html',log_trx=data_trx)

# - ROUTING TO REALTIME PAGE
@app.route('/video')
def realtime():
    return render_template('video.html')

# - ROUTING TO REALTIME PAGE
@app.route('/model_ml')
def model_lm():
    return render_template('model_ml.html')

if __name__ =="__main__":
    app.run(debug=True)
    