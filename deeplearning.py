#------------------------------------------------------------\
#  @2022 - 22 Nov 
#  Createed By A. Toto Priyono
#  DEEP LEARNING - YOLO & TENSORFLOW
#
#  Demo Implementasi Plate Detection & Plate Recognition
#  Using Yolo & Tensorflow - CNN Algorithm
#-----------------------------------------------------------\

import numpy as np
import cv2
import matplotlib.pyplot as plt
import os
#import pytesseract as pt
import shutil
import tensorflow as tf
from tensorflow.keras.models import load_model

os.environ['TF_FORCE_GPU_ALLOW_GROWTH'] = "true"

# SETUP CONFIG
INPUT_WIDTH =  640
INPUT_HEIGHT = 640

MODEL_PLATE_DETECTION ="best.onnx"
MODEL_CHAR_RECOGNITION ="plate_recognition_model_2022-12-10-173722.h5"



# LOAD YOLO MODEL FOR OBJECT DETECTION
# THE MODEL COME FROM TRAINING PROCESS
net = cv2.dnn.readNetFromONNX('./static/model/weights/'+MODEL_PLATE_DETECTION)
net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
net.setPreferableTarget(cv2.dnn.DNN_TARGET_CPU)


# FUNCTION FOT OBJECT DETECTION

def get_detections(img):

    #CONVERT IMAGE TO YOLO FORMAT
    image=img.copy()
    row, col, d = image.shape
    max_rc = max(row,col)
    input_image=np.zeros((max_rc,max_rc,d),dtype=np.uint8)
    input_image[0:row,0:col]=image

    #cv2.namedWindow('Test Image',cv2.WINDOW_KEEPRATIO)
    #cv2.imshow('Test Image',input_image)
    #cv2.waitKey()
    #cv2.destroyAllWindows()

    #GET PREDECTION FROM YOLO MODEL
    blob = cv2.dnn.blobFromImage(input_image,1/255,(INPUT_WIDTH,INPUT_HEIGHT),swapRB=True,crop=False)
    net.setInput(blob)
    preds = net.forward()
    detections = preds[0]

    return input_image,detections

def non_maximum_supression(input_image, detections):
    #FILTER DETECTION BASE ON CONFIDENT 
    #center x, center y, w, h, conf, proba
    boxes =[]
    confidences =[]

    image_w, image_h = input_image.shape[:2]
    x_factor = image_w/INPUT_WIDTH
    y_factor = image_h/INPUT_HEIGHT

    for i in range(len(detections)):
        row=detections[i]
        confidence=row[4]
        if confidence > 0.4:
            class_score=row[5]
            if class_score > 0.25:
                cx,cy,w,h=row[0:4]
                left =int((cx-0.5*w)*x_factor)
                top =int((cy-0.5*h)*y_factor)
                width =int(w*x_factor)
                height=int(h*y_factor)
                box =np.array([left,top,width,height])

                confidences.append(confidence)
                boxes.append(box)

    #CLEAN
    boxes_np =np.array(boxes).tolist()
    confidences_np=np.array(confidences).tolist()
    #NMS
    #index =cv2.dnn.NMSBoxes(boxes_np,confidences_np,0.25,0.45).flatten()  
    index =cv2.dnn.NMSBoxes(boxes_np,confidences_np,0.25,0.45)  
    
    return boxes_np, confidences_np, index

def apply_brightness_contrast(input_img, brightness = 0, contrast = 0):
    
        if brightness != 0:
            if brightness > 0:
                shadow = brightness
                highlight = 255
            else:
                shadow = 0
                highlight = 255 + brightness
            alpha_b = (highlight - shadow)/255
            gamma_b = shadow
            
            buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
        else:
            buf = input_img.copy()
        
        if contrast != 0:
            f = 131*(contrast + 127)/(127*(131-contrast))
            alpha_c = f
            gamma_c = 127*(1-f)
            
            buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)

        return buf

def extract_roi(image, bbox,file_name):
    x,y,w,h = bbox
    
    #crop tnkb
    tnkb = image[y:y+h,x:x+w]
    cv2.imwrite('./static/hasil_prediksi/tnkb/{}'.format(file_name),tnkb)
 
    if 0 in tnkb.shape:
        return 'not detected'
    else:
        #pt.pytesseract.tesseract_cmd = 'C:/Users/LENOVO/AppData/Local/Tesseract-OCR/tesseract.exe'
        
        tnkb_bgr = cv2.cvtColor(tnkb,cv2.COLOR_RGB2BGR)
        roi = cv2.cvtColor(tnkb_bgr,cv2.COLOR_BGR2GRAY)
        #roi = apply_brightness_contrast(roi,brightness=40,contrast=70)
        #text = pt.image_to_string(magic_color)
        #text = pt.image_to_string(roi,lang='eng',config='--psm 6')
        #text = pt.image_to_string(roi)
        #license_text = text.strip()
        license_text = "xxxx"
        
        return license_text,roi
    
def drawings(image, boxes_np,confidences_np,index,file_name):
    #Draw boxes
    for ind in index:
        x,y,w,h = boxes_np[ind]
        bb_conf = confidences_np[ind]
        #conf_text ='Conf Level:{:.0f}%'.format(bb_conf*100)
        license_text,roi = extract_roi(image, boxes_np[ind],file_name)

        cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),5)
        #cv2.rectangle(image,(x,y-40),(x+w,y),(0,255,0),-1)
        #cv2.rectangle(image,(x,y+h),(x+w,y+h+60),(0,0,0),-1)
        
        #cv2.putText(image,conf_text,(x,y-15),cv2.FONT_HERSHEY_SIMPLEX,0.8,(255,255,255),2)
        #cv2.putText(image,license_text,(x,y+h+40),cv2.FONT_HERSHEY_SIMPLEX,0.7,(255,255,255),2)

    #cv2.namedWindow('Result',cv2.WINDOW_KEEPRATIO)
    #cv2.imshow('Result',image)
    #cv2.waitKey()
    #cv2.destroyAllWindows()
    
    return image,roi,license_text,bb_conf

def get_tnkb_yolo(upload_path):
    img = cv2.imread(upload_path)
    file_name = os.path.basename(upload_path)
    
    #Proses deteksi pelat
    input_image,detections = get_detections(img)
    
    #Proses seleksi hasil deteksi pelat,
    boxes_np,confidences_np, index = non_maximum_supression(input_image,detections)
    
    #Proses penggambaran hasil seleksi posisi pelat ke image asli
    result_img,roi,license_text,bb_conf=drawings(img,boxes_np,confidences_np,index,file_name)
    bb_conf= round(bb_conf*100)
    #Proses menyimpan hasil deteksi pada image asli, hasil crop pelat kendaraan
    #code ...
    cv2.imwrite('./static/hasil_prediksi/obj_detection/{}'.format(file_name),result_img)
    #cv2.imwrite('./static/hasil_prediksi/tnkb/{}'.format(file_name),roi)
    
    #output berupa nama file, text nomor pelar, conf level
    return roi,bb_conf


### Mencari dan Menentukan Countour karakter 

def find_contours(dimensions, img,file_name) :

    # Find all contours in the image
    cntrs, _ = cv2.findContours(img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    # Retrieve potential dimensions
    lower_width = dimensions[0]
    upper_width = dimensions[1]
    lower_height = dimensions[2]
    upper_height = dimensions[3]
    
    # Check largest 5 or  15 contours for license plate or character respectively
    cntrs = sorted(cntrs, key=cv2.contourArea, reverse=True)[:15]
    
    ii = cv2.imread('./static/hasil_prediksi/countur/{}'.format(file_name))
    
    x_cntr_list = []
    target_contours = []
    img_res = []
    for cntr in cntrs :
        # detects contour in binary image and returns the coordinates of rectangle enclosing it
        intX, intY, intWidth, intHeight = cv2.boundingRect(cntr)
        
        # checking the dimensions of the contour to filter out the characters by contour's size
        if intWidth > lower_width and intWidth < upper_width and intHeight > lower_height and intHeight < upper_height :
            x_cntr_list.append(intX) #stores the x coordinate of the character's contour, to used later for indexing the contours

            char_copy = np.zeros((44,24))
            # extracting each character using the enclosing rectangle's coordinates.
            char = img[intY:intY+intHeight, intX:intX+intWidth]
            char = cv2.resize(char, (20, 40))
            
            cv2.rectangle(ii, (intX,intY), (intWidth+intX, intY+intHeight), (0,255,0), 2)
            plt.imshow(ii, cmap='gray')

            # Make result formatted for classification: invert colors
            char = cv2.subtract(255, char)

            # Resize the image to 24x44 with black border
            #char_copy[2:42, 2:22] = char
            #char_copy[0:2, :] = 0
            #char_copy[:, 0:2] = 0
            #char_copy[42:44, :] = 0
            #char_copy[:, 22:24] = 0

            img_res.append(char) # List that stores the character's binary image (unsorted)
            
    # Return characters on ascending order with respect to the x-coordinate (most-left character first)
            
    #plt.show()
    cv2.imwrite('./static/hasil_prediksi/segmentasi/{}'.format(file_name),ii)
     
    # arbitrary function that stores sorted list of character indeces
    indices = sorted(range(len(x_cntr_list)), key=lambda k: x_cntr_list[k])
    img_res_copy = []
    for idx in indices:
        img_res_copy.append(img_res[idx])# stores character images according to their index
    img_res = np.array(img_res_copy)

    return img_res

### Segmentasi Karakter

def segment_characters(image,file_name) :

    # Preprocess cropped license plate image
    img_lp = cv2.resize(image, (333, 75))

    #img_gray_lp = cv2.cvtColor(img_lp, cv2.COLOR_BGR2GRAY)
    #Bila plate hitam, ubah ke putih
    #img_lp = cv2.GaussianBlur(img_lp,(5,5),0)
    _, img_binary_lp = cv2.threshold(img_lp,200, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    jml_pix_putih = np.sum(img_binary_lp == 255)
    jml_pix_hitam = np.sum(img_binary_lp == 0)
    if (jml_pix_hitam>jml_pix_putih):
        img_binary_lp=np.invert(img_binary_lp)
    
    img_binary_lp = cv2.erode(img_binary_lp, (3,3))
    img_binary_lp = cv2.dilate(img_binary_lp, (3,3))

    LP_WIDTH = img_binary_lp.shape[0]
    LP_HEIGHT = img_binary_lp.shape[1]

    # Make borders white
    img_binary_lp[0:2,:] = 255
    img_binary_lp[:,0:15] = 255
    img_binary_lp[60:75,:] = 255
    img_binary_lp[:,318:333] = 255

    # Estimations of character contours sizes of cropped license plates
    dimensions = [LP_WIDTH/10,
                       LP_WIDTH/2,
                       LP_HEIGHT/10,
                       2*LP_HEIGHT/5]
    #plt.imshow(img_binary_lp, cmap='gray')
    #plt.show()
    cv2.imwrite('./static/hasil_prediksi/countur/{}'.format(file_name),img_binary_lp)

    # Get contours within cropped license plate
    char_list = find_contours(dimensions, img_binary_lp,file_name)

    return char_list

## split image char & save to directory

def  get_image_char(char,file_name):
    dir_name =file_name.split('.')
    dir_name = dir_name[0]
    list_image_char=[]
    
    dir_path='./static/hasil_prediksi/segmentasi/char_'+dir_name
    if os.path.exists(dir_path):
        shutil.rmtree(dir_path)
    
    os.mkdir(dir_path)
    
    jml_countur = len(char)
    for i in range(jml_countur):
        plt.subplot(1, jml_countur, i+1)
        plt.imshow(char[i], cmap='gray')
        plt.axis('off')
        cv2.imwrite(dir_path + '/{}.jpg'.format(i),char[i])
        list_image_char.append(dir_path+'/{}.jpg'.format(i))
    
    return list_image_char


# Metric yang digunakan untuk pengecekan pada saat proses training
# Character Recognition

def f1score(y, y_pred):
    return f1_score(y, tf.math.argmax(y_pred, axis=1), average='micro') 

def custom_f1score(y, y_pred):
    return tf.py_function(f1score, (y, y_pred), tf.double)


# LOAD TENSORFLOW MODEL FOR CHARACTER RECOGNITION
# THE MODEL COME FROM TRAINING PROCESS

#file_path = './static/model/char_recognition/char_recognition_model.h5'
#file_path = './static/model/char_recognition/char_recognition_model_improve2.h5'
file_path = './static/model/char_recognition/'+MODEL_CHAR_RECOGNITION

my_model=load_model(file_path,custom_objects={"custom_f1score": custom_f1score })

# Prediksi hasil pembacaan karakter
def fix_dimension(img): 
    new_img = np.zeros((28,28,3))
    for i in range(3):
        new_img[:,:,i] = img
    return new_img
  
def show_results(img_char):
    dic = {}
    characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    for i,c in enumerate(characters):
        dic[i] = c

    output = []
    for i,ch in enumerate(img_char): #iterating over the characters
        img_ = cv2.resize(ch, (28,28), interpolation=cv2.INTER_AREA)
        img = fix_dimension(img_)
        img = img.reshape(1,28,28,3) #preparing image for the model
        y_= my_model.predict(img)
        y_=np.argmax(y_,axis=1)[0]
        character = dic[y_]
        output.append(character) #storing the result in a list
        
    plate_number = ''.join(output)
    
    return plate_number