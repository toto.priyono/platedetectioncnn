import matplotlib.pyplot as plt
import numpy as np
import cv2
import tensorflow as tf
from sklearn.metrics import f1_score 
from tensorflow.keras import optimizers
from tensorflow.keras.models import Sequential, save_model, load_model
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.layers import Dense, Flatten, MaxPooling2D, Dropout, Conv2D
from datetime import datetime
import tensorflow.keras.backend as K


print("===========================================================")
print("=  Training Script for Character Recognition              =")
print("=  By A. Toto Priyono                                     =")
print("=                                                         =")
print("=  The Output for this script is Model                    =")
print("=  The Model will be used to predict image of character   =")
print("=  Training can be execute everytime to update Model      =")
print("===========================================================")

input("\n\nTekan Enter untuk memulai proses training...\n\n")

today=datetime.now()
now=today.strftime("%Y-%m-%d-%H%M%S")
print(now)

train_datagen = ImageDataGenerator(rescale=1./255, width_shift_range=0.1, height_shift_range=0.1)
path = './dataset_alphanumeric/data'
train_generator = train_datagen.flow_from_directory(
        path+'/train',  # this is the target directory
        target_size=(28,28),  # all images will be resized to 28x28
        batch_size=1,
        class_mode='sparse')

validation_generator = train_datagen.flow_from_directory(
        path+'/val',  # this is the target directory
        target_size=(28,28),  # all images will be resized to 28x28 batch_size=1,
        class_mode='sparse')

# Metrics for checking the model performance while training
def f1score(y, y_pred):
    return f1_score(y, tf.math.argmax(y_pred, axis=1), average='micro') 

def custom_f1score(y, y_pred):
    return tf.py_function(f1score, (y, y_pred), tf.double)

print("Log: Membuat Model...")

K.clear_session()
model = Sequential()
model.add(Conv2D(16, (22,22), input_shape=(28, 28, 3), activation='relu', padding='same'))
model.add(Conv2D(32, (16,16), input_shape=(28, 28, 3), activation='relu', padding='same'))
model.add(Conv2D(64, (8,8), input_shape=(28, 28, 3), activation='relu', padding='same'))
model.add(Conv2D(64, (4,4), input_shape=(28, 28, 3), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(4, 4)))
model.add(Dropout(0.4))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dense(36, activation='softmax'))

model.compile(loss='sparse_categorical_crossentropy', optimizer=optimizers.Adam(learning_rate=0.0001), metrics=[custom_f1score])
print("Log: Arsitektur Layer Model ...")
model.summary()

class stop_training_callback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs={}):
        if(logs.get('val_custom_f1score') > 0.20):
            self.model.stop_training = True
            
print("Log: Proses Training...\n\n")
            
batch_size = 1
callbacks = [stop_training_callback()]
model.fit(
      train_generator,
      steps_per_epoch = train_generator.samples // batch_size,
      validation_data = validation_generator, 
      epochs = 80, verbose=1, callbacks=callbacks)

print("\n\nLog: Proses Training Selesai ...Nilai Akurasi telah mencapai 99%")
file_model = 'plate_recognition_model_' + now + '.h5'
filepath = './dataset_alphanumeric/model/'+file_model
print("Log: Membuat model baru : " + file_model)
save_model(model, filepath)
print("Log: Model baru telah disimpan di " + filepath)